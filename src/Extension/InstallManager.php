<?php
namespace Cms\Api\Install\Extension;

use Zend\Json\Json;
use Zend\Http\Client;
use Zend\Config\Config;
use Zend\Stdlib\Parameters;
use Zend\Validator\File\Exists;
use Doctrine\ORM\Tools\ToolsException;
use Cms\ExtensionManager\Extension\ResponderEvent;
use Cms\ExtensionManager\Extension\AbstractExtension;

class InstallManager extends AbstractExtension {

    const INSTALLTION = 'installation.event';

    protected $defaultAdmin = array();

    public function installationEvent(ResponderEvent $e) {
        
        $params   = $e->getParams();
        $params   = $this->extractDefaultAdmin($params);
        
        $triggers = array('cms.config.create', 'cms.schema.create');
        foreach($triggers as $index => $trigger) {
            $responder = $this->trigger($trigger, $params->toArray());
            if($responder->isError()) {
                return $responder;
            }
        }
        
        $responder = $this->trigger('cms.auth.create.user', $this->defaultAdmin);
        if($responder->isError()) {
            return $responder;
        }

        $config = $this->trigger('get.cms.config')->getConfig();
        $server = $config->bas_cms->config->get('servers');
        return $e->responder(null, false, 'Succesfully Completed Installation', array('servers' => $server->toArray(), 'user' => $responder->getData()), 200, true);
    }

    protected function getDefaultCredentials() {
        $params['email']    = $this->defaultAdmin['email'];
        $params['password'] = $this->defaultAdmin['password'];

        return $params;
    }

    protected function extractDefaultAdmin(array $params) {
        
        $params = new Parameters($params);

        $this->defaultAdmin = array(
                'roles'            => $params->get('userrole', 'superman'),
                'email'            => $params->get('email', 'admin@admin.com'),
                'password'         => $params->get('password', 'Reset this password!'),
                'password_confirm' => $params->get('password_confirm', 'Reset this password!')
        );

        foreach(array('email', 'password', 'password_confirm') as $index => $key) {
            if(array_key_exists($key, $params)) {
                unset($params[$key]);
            }
        }
        
        return $params;
    }
}