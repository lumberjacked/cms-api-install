<?php

return array(

    'service_manager' => include 'service.config.php',

    'bas_cms' => array(
        'extensions' => array(
            'install-manager' => array(
                'type'    => 'Cms\Api\Install\Extension\InstallManager',
                'options' => array(
                    'listeners' => array(
                        'installation.event' => 'installationEvent'
                    )
                ),
            )
        )
    )
);